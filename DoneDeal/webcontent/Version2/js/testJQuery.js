
/*
An upgraded version of the random Beer App this using Jquery
Writing by Ruairi Whelan Dated(17/07/16)
*/
var drink = [];
var rand;
		
$(document).ready(function(){
     fillTestingArray();
	 //testing();
	//randomTest();
});

function testing(){
	// legacy code for testing purposes 
    // modify Index.html to have new Name, Description ect to work with Jquery 
		for (var i= 0; i < drink.length; i++){
		  $("#Test1").before(drink[i].Name + " "+ drink[i].des + "" + drink[i].PerAlc +" "+drink[i].Location +"<br/>");
		}
}

function randomTest(){
	rand = Math.floor(Math.random()* drink.length);
    updateScreen();
}
function updateScreen(){
	      $("#dename").html(drink[rand].Name);
		  $("#deDesc").html(drink[rand].des); // sometimes this will not changes when updated - turned out I had it Des, instead of des in the creation statement
		  $("#percentage").html(drink[rand].PerAlc);
		  $("#Locat").html(drink[rand].Location);
}
function fillTestingArray(){
         var newdrink = {Name:"Deise gold ",des:"the only true gold of Waterford", PerAlc: "3.5%",Location:"Dunmore, Waterford"};
		 var newdrink2 ={Name:"Golden Due",des:"It is golden so enjoy", PerAlc: "3.5%",Location:"Rosslare, Wexford"};
		 var newdrink3 = {Name:"Miller's own",des:"the only drink the miller will drink", PerAlc: "5.3%",Location:"Scarrif, Clare"};
		 var newdrink4 = {Name:"Golden Due",des:"It is golden so enjoy", PerAlc: "3.5%",Location:"Rosslare, Wexford"};
		 var newdrink5 = {Name:"Hobgoblin",des:"A craft beer brewed in Ireland and England, noticed for it's light refreshing taste.", PerAlc: "5.3%",Location:"Scarrif, Clare"};
		 var newdrink6 = {Name:"The lord's reserve",des:"a Drink fit only for the greatest of nobles", PerAlc: "3.5%",Location: "Ardattin, Carlow"};
		 var newdrink7 = {Name:"testBeer 1 ",des:"it's a test beer to populate an array ", PerAlc: "6%",Location: "Dream land, Sleepvill, nightland"};
		 var newdrink8 = {Name:"testBeer 2 ",des:"it's a 2nd test beer to populate an array ", PerAlc: "6%",Location: "Dragonberg, Sleepvill, nightland"};
		 
		 drink.push(newdrink);
		 drink.push(newdrink2);
		 drink.push(newdrink3);
		 drink.push(newdrink4);
		 drink.push(newdrink5);
		 drink.push(newdrink6);
		 drink.push(newdrink7);
		 drink.push(newdrink8);
		 
}
